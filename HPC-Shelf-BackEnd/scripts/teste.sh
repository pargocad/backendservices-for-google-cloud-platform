#!/bin/bash
nodes="address"
file=$(cat $nodes)
for line in $file
do
  case $line in '') ;; *)
     sleep 10; echo $line &
  esac
done;

# Wait for all parallel jobs to finish
while [ 1 ] ; do fg 2> /dev/null; [ $? == 1 ] && break; done
