﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Services;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Compute.v1;
using Google.Apis.Services;
using Newtonsoft.Json;
using System.Threading.Tasks;
using Google.Apis.Compute.v1.Data;

namespace Tests {
    public class MainClass {

        public static void Main(string[] args) 
        {
			ComputeService computeService = new ComputeService(new BaseClientService.Initializer
			{
				HttpClientInitializer = GetCredential(),
				ApplicationName = "HPC Shelf - Backend",
			});

			// Project ID for this request.
			string project = "firm-reef-239023";  // TODO: Update placeholder value.

			// The name of the zone for this request.
			string default_zone = "us-east1-c";  // TODO: Update placeholder value.

            ImagesResource.GetRequest request_image = computeService.Images.Get(project, "hpcshelf-virtualplatform-image-10");
            Image image = request_image.Execute();

            NetworksResource.GetRequest get_network = computeService.Networks.Get(project, "default");
            Network network = get_network.Execute();

            NetworkInterface network_interface = new NetworkInterface();
            network_interface.Network = network.SelfLink;

            AccessConfig access_config = new AccessConfig();
            network_interface.AccessConfigs = new List<AccessConfig>();
            network_interface.AccessConfigs.Add(access_config);

            for (int j = 0; j <= 2; j++)
            {
				Disk disk_description = new Disk();
				disk_description.SourceImage = image.SelfLink;
				disk_description.Name = "hpcshelf-disk-" + j;
				DisksResource.InsertRequest request_disk = computeService.Disks.Insert(disk_description, project, default_zone);
				request_disk.Execute();

                DisksResource.GetRequest get_disk = computeService.Disks.Get(project, default_zone, "hpcshelf-disk-" + j);
				Disk new_disk = get_disk.Execute();

				AttachedDisk disk = new AttachedDisk();
				disk.Source = new_disk.SelfLink;
				disk.Boot = true;
				
                Instance requestBody = new Instance();

                requestBody.Name = "hpcshelf-virtualplatform-" + j;
                requestBody.Zone = default_zone;
                requestBody.MachineType = "zones/us-east1-c/machineTypes/n1-standard-1";

                requestBody.Disks = new List<AttachedDisk>();
                requestBody.Disks.Insert(0, disk);

                requestBody.NetworkInterfaces = new List<NetworkInterface>();
                requestBody.NetworkInterfaces.Insert(0, network_interface);

                InstancesResource.InsertRequest request = computeService.Instances.Insert(requestBody, project, default_zone);

                // To execute asynchronously in an async method, replace `request.Execute()` as shown:
                // Operation response = await request.ExecuteAsync();
                Operation response = request.Execute();

				InstancesResource.GetRequest get_instance = computeService.Instances.Get(project, default_zone, requestBody.Name);
				Instance instance = get_instance.Execute();

                Console.Write(instance.Hostname);
			}
   		}

		public static GoogleCredential GetCredential()
		{
			GoogleCredential credential = Task.Run(() => GoogleCredential.GetApplicationDefaultAsync()).Result;
			if (credential.IsCreateScopedRequired)
			{
				credential = credential.CreateScoped("https://www.googleapis.com/auth/cloud-platform");
			}
			return credential;
		}
	}
}
